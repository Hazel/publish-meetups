# Publish Meetups

Publish Meetups is a project that enables the **automated** publication of public meetups and appointments on social media platforms like Twitter and Mastodon.

## Supported Platforms

- [ ] Twitter
- [ ] Mastodon
