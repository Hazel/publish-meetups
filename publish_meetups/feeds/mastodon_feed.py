from pathlib import Path

from mastodon import Mastodon

from . import Feed
from ..utils import PROGRAM_DATA_DIR, prompt, config


class MastodonFeed(Feed):
    __config_name__ = "mastodon"
    CLIENTCRED_PATH: Path = PROGRAM_DATA_DIR.joinpath("mastodon_clientcred.secret")

    @classmethod
    def prompt_auth(cls) -> dict:
        """
        mastodon needs:
        - the instance used
        - an access token
        """

        return {
            "api_base_url": prompt.for_string("The instance you use", "https://mastodon.social"),
            "access_token": prompt.for_password("Access token"),
        }

    # https://github.com/halcy/Mastodon.py
    def __init__(self, api_base_url: str, access_token: str, **kwargs):
        self.mastodon = Mastodon(
            api_base_url=api_base_url,
            access_token=access_token,
        )

        super().__init__()

    def post(self, message: str):
        self.mastodon.toot(message)

        super().post(message)
