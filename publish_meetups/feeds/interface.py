import logging


class Feed:
    __config_name__ = "feed"

    @classmethod
    def prompt_auth(cls, existing_config: dict) -> dict:
        return existing_config

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)


    def post(self, message: str):
        self.logger.info(f"Posting message to {self.__class__.__name__}: {message}")
        pass