from . import Routine
from ..feeds import Feed, TwitterFeed
from ..utils import date

from datetime import datetime
from ics import Calendar


class Development(Routine):
    def test_feeds(self):
        for feed in self.iter_feeds():
            feed_instance = self.init_feed(feed)
            feed_instance.post(f"Hello World!!! ({datetime.now()})")

    def run(self):
        self.test_feeds()
