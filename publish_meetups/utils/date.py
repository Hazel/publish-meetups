from datetime import datetime
from ics import Calendar, Event

from . import config


def to_date_string(date_time: datetime) -> str:
    date_format = config.get_field("date_format", "%Y-%m-%d %H:%M:%S")

    weekday_translations = config.get_field("weekday_translations", {})
    months_translations = config.get_field("months_translations", {})

    parsed = date_time.strftime(date_format)

    for key, value in weekday_translations.items():
        parsed = parsed.replace(key, value)
    
    for key, value in months_translations.items():
        parsed = parsed.replace(key, value)

    return parsed.strip()


def to_time_string(date_time: datetime) -> str:
    time_format = config.get_field("time_format", "%H:%M")
    time_format_full_hour = config.get_field("time_format_full_hour", time_format)

    parsed = date_time.strftime(time_format)
    if int(date_time.minute) == 0:
        parsed = date_time.strftime(time_format_full_hour)

    return parsed.strip()


def event_formatting_values(event: Event, locale="en") -> dict:
    return {
        "time": to_time_string(event.begin),
        "date": to_date_string(event.begin),
        "date_humanized": event.begin.humanize(locale=locale),
    }
