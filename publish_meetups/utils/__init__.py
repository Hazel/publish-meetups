import logging
from pathlib import Path

import platformdirs


logging.basicConfig(level=logging.INFO)
PROGRAM_NAME: str = "publish-meetups"
PROGRAM_DATA_DIR: Path = platformdirs.user_config_path(appname=PROGRAM_NAME)
PROGRAM_DATA_DIR.mkdir(parents=True, exist_ok=True)

ICS_FILE = Path(PROGRAM_DATA_DIR / "meetup.ics")


__all__ = [
    "prompt", 
    "PROGRAM_DATA_DIR", 
    "PROGRAM_NAME", 
    "errors", 
    "config", 
    "ICS_FILE",
    "date",
]
