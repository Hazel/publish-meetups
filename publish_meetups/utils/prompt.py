from typing import Optional

from getpass import getpass


def for_string(msg: str, default: Optional[str] = None) -> str:
    def _real_prompt() -> str:
        nonlocal msg, default

        if default is not None:
            return input(f"{msg} [{default}]: ").strip() or default

        return input(msg + ': ').strip()
    
    result = _real_prompt()
    print("> " + result)
    return result


def for_password(msg: str) -> str:
    return getpass(msg + ': ').strip()
