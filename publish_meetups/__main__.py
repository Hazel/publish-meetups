from .routines.development import Development


def cli():
    Development().run()


if __name__ == "__main__":
    cli()
